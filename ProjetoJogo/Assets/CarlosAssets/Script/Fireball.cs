﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Fireball : MonoBehaviour
{
    public int damage;
    public float speed;

    private GameObject player;
    private Vector3 randomBullet;

    private void Awake()
    {
        //gera uma variação randomica no eixo x
        randomBullet = new Vector3(Random.Range(-0.1f, 0.1f), 0, 0) + Vector3.forward;
        player = GameObject.FindWithTag("Player");
    }

    void Update()
    {
        transform.Translate(randomBullet * speed * Time.deltaTime);
    }

    void OnTriggerEnter(Collider objeto)
    {
        if (objeto.tag == "Player")
        {
            player.GetComponent<PlayerBehaviour>().health -= damage;

            if (player.GetComponent<PlayerBehaviour>().health <= 0)
            {
                //Destroy(objeto.gameObject);
                Time.timeScale = 0;
            }

        }
        Destroy(gameObject.GetComponentInChildren<ParticleSystem>());
        Destroy(gameObject);
    }
}
