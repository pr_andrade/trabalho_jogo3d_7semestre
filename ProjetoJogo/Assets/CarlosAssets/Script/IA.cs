﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IA : MonoBehaviour {

	// Controle da vida
	//public float vidaInimigo = 100;

	//Controladores IA: 
	float distancia;
	public Transform player;

	float TempoDeTiro;
	public GameObject Bala;
	public GameObject CanoDaArma;


	void Update () {

		distancia = Vector3.Distance(transform.position, player.position);

		TempoDeTiro += Time.deltaTime;

		if(TempoDeTiro>0.9) {
            Instantiate(Bala, CanoDaArma.transform.position, CanoDaArma.transform.rotation);
            TempoDeTiro = 0;
		}

		if (distancia < 40) {

			if (distancia > 5) {
				seguir();
			}
			else {
				GetComponent<Animator>().SetBool("MovendoParaFrente", false);
			}
			olhar();
		}
	}

	void olhar() {
		Quaternion visao;
		visao = Quaternion.LookRotation(player.position - transform.position); // Pra onde olhar
		transform.rotation = Quaternion.Slerp(transform.rotation, visao, 6f * Time.deltaTime); //Como olhar
	}

	void seguir() {
		transform.Translate(Vector3.forward * 0.5f * Time.deltaTime);
		GetComponent<Animator>().SetBool("MovendoParaFrente", true);
	}


	// Calculo de dados 
	//public void dano(float quantidadeDano){
	//
	//	vida -= quantidadeDano;
	//	if (vida <= 0){
	//		Destroy(gameObject);
	//	}
	//
	//}


}
