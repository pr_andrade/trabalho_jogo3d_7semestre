﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerBehaviour : MonoBehaviour
{
    public List<BaseWeapon> weapons;
    public float health;
    public Texture healthBar_health;
    public Texture healthBar_bar;

    private int maximumHealth = 100;
    private float currentHealth;

    // Use this for initialization
    void Start()
    {
        health = maximumHealth;
    }

    // Update is called once per frame
    void Update()
    {
        InputPlayer();
        if(health >= maximumHealth)
        {
            health = maximumHealth;
        }
        else if (health <= 0)
        {
            health = 0;
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
        }
        currentHealth = health;
    }

    public void InputPlayer()
    {
        if ((Input.GetButton("Fire1") && weapons[0].isAutomatic) || Input.GetButtonDown("Fire1"))
        {
            weapons[0].Fire();        }

        if (Input.GetKeyDown(KeyCode.R))
        {
            weapons[0].Reload();
        }

        if (Input.GetButton("Fire2"))
        {
            weapons[0].EnableZoom();
        }
        else
        {
            weapons[0].DisableZoom();
        }        
    }

    void OnGUI()
    {
        GUI.DrawTexture(new Rect(Screen.width / 9.64f, Screen.height / 7.05f, Screen.width / 7.5f / maximumHealth * currentHealth, Screen.height / 55), healthBar_health);
        GUI.DrawTexture(new Rect(Screen.width / 90, Screen.height / 20, Screen.width / 4, Screen.height / 5), healthBar_bar);
    }
}
