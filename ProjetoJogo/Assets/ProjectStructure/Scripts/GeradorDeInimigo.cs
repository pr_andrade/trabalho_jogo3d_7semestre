﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GeradorDeInimigo : MonoBehaviour {

	public GameObject RoboInimigo;
	public float quantidade;
	private float contadorTempo = 0;
	public float tempoGerarInimigo = 1;



	void Start() {
		RoboInimigo = GameObject.FindWithTag("Inimigo");
	}


	// Update is called once per frame
	void Update () {

		contadorTempo+= Time.deltaTime;

		if (RoboInimigo) {

			if (contadorTempo >= tempoGerarInimigo && quantidade > 0) {

				Instantiate(RoboInimigo, transform.position, transform.rotation);
				contadorTempo=0;
				quantidade--;

			}
			
		}
		
	}
}
