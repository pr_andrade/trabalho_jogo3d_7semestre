﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AssaultRifleBehaviour : BaseWeapon
{
    AudioSource fireWeaponAudio;

    void Awake()
    {
        fireWeaponAudio = GetComponent<AudioSource>();
    }

    // Use this for initialization
    new void Start()
    {
        base.Start();
    }

    // Update is called once per frame
    new void Update()
    {
        base.Update();
    }
    protected override void OnFire(int damage, float range, Vector3 startPosition)
    {
        //cria a bala na direção apontada
        GameObject tempBullet = Instantiate(bullet.gameObject, positionSpawnProjetctil.position, transform.rotation);

        Ray rayShot = new Ray(startPosition, transform.forward);
        RaycastHit shootHit;

        if (Physics.Raycast(rayShot, out shootHit, range))//out coleta as informações colisão e salva no hitInfo
        {
            if (shootHit.collider.tag.Equals("Inimigo") || shootHit.collider.tag.Equals("Boss"))
            {
                shootHit.collider.GetComponent<EnemyBehaviour>().TakeDamage(damage, shootHit);
            }
            else
            {
                Destroy(InstatiateDecalShotPrefab(shootHit), 5);
            }
            Destroy(tempBullet, 0.05f);
        }
        fireWeaponAudio.Play();
    }

    protected override void OnReload()
    {

    }


}
