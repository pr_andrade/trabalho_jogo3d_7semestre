﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.SceneManagement;

public class EnemyBehaviour : MonoBehaviour
{

    public float health;
    public float fireRateTime;
    public GameObject bullet;
    public GameObject canoDaArma;

    private float currentfireRateTime;
    private bool canFire = true;
    private bool isDead = false;
    private ParticleSystem hitParticles;
    private float currentHealth;
    private Transform player;
    public GameObject explosion;
    private NavMeshAgent nav;
    private Animator animator;

    // Use this for initialization
    void Awake()
    {
        player = GameObject.FindGameObjectWithTag("Player").transform;
        nav = GetComponent<NavMeshAgent>();
        hitParticles = GetComponentInChildren<ParticleSystem>();
        animator = GetComponent<Animator>();
        currentHealth = health;
    }

    // Update is called once per frame
    void Update()
    {
        float distancia = Vector3.Distance(transform.position, player.transform.position);

        currentfireRateTime += Time.deltaTime;
        if (currentfireRateTime > fireRateTime)
            canFire = true;
        else
            canFire = false;

        nav.SetDestination(player.position);

        //se estiver perto
        if (distancia < 100)
        {
            if (!isDead)
                nav.isStopped = false;

            Idle(false);
            MovimentarParaFrente(true);

            if (distancia < 20 && distancia > nav.stoppingDistance)
            {
                InstantiateBullet();
            }
            else if (distancia <= nav.stoppingDistance)
            {
                Olhar();
                InstantiateBullet();
                MovimentarParaFrente(false);
            }
        }
        //se estiver longe
        else
        {
            nav.isStopped = true;
            MovimentarParaFrente(false);
            Idle(true);
        }
    }

    void InstantiateBullet()
    {
        if (canFire && !isDead)
        {
            Instantiate(bullet.gameObject, canoDaArma.transform.position, canoDaArma.transform.rotation);
            currentfireRateTime = 0;
        }
    }

    void MovimentarParaFrente(bool condition)
    {
        if (!isDead)
            animator.SetBool("MovendoParaFrente", condition);
    }

    void Idle(bool condition)
    {
        if (!isDead)
            animator.SetBool("Idle", condition);
    }

    void Olhar()
    {
        Quaternion visao;
        visao = Quaternion.LookRotation(player.position - transform.position); // Pra onde olhar
        transform.rotation = Quaternion.Slerp(transform.rotation, visao, 6f * Time.deltaTime); //Como olhar
    }

    public void TakeDamage(int damage, RaycastHit shootHit)
    {
        //posiciona a particula de hit onde ocorreu a colisão
        hitParticles.transform.position = shootHit.point;
        //ativa a animação de particula
        hitParticles.Play();

        currentHealth -= damage;

        if (currentHealth <= 0)
        {
            Dead(shootHit);
            if (gameObject.tag == "Boss")
            {
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 2);
            }
        }
    }

    void Dead(RaycastHit shootHit)
    {
        nav.isStopped = true;
        isDead = true;
        MovimentarParaFrente(false);
        animator.Play("die_back_rest");
        Instantiate(explosion.gameObject, transform.position, transform.rotation);
        Destroy(GameObject.Find(shootHit.collider.name).gameObject, 2);
    }
}
