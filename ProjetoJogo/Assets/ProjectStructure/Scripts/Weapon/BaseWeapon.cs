﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public abstract class BaseWeapon : MonoBehaviour
{

    public int damage;
    public float range;
    public float fireRateTime;
    public int capacityAmmo;
    public int capacityAmmoOutWeapon;
    public Transform positionSpawnProjetctil;
    public ParticleSystem fireParticle;
    public bool isAutomatic;
    public float aimZoom;
    public float reloadTime;
    public InputField inputFieldCurrentCapacityAmmo;
    public InputField inputFieldCurrentCapacityAmmoOutWeapon;

    private int currentAmountAmmo;
    private int currentAmountAmmoOutWeapon;
    private float currentfireRateTime;
    private bool canFire = true;
    private float startZoom;
    private float waitToFireAfterReload;
    private ParticleSystem currentFireParticle;

    public BulletBehaviour bullet;
    public GameObject decalShotPrefab;

    // Use this for initialization
    protected void Start()
    {
        waitToFireAfterReload = 0;
        currentAmountAmmo = capacityAmmo;
        currentAmountAmmoOutWeapon = capacityAmmoOutWeapon;
        startZoom = Camera.main.fieldOfView;
        GameObject tempParticle = Instantiate(fireParticle.gameObject, positionSpawnProjetctil.position, transform.rotation);
        currentFireParticle = tempParticle.GetComponent<ParticleSystem>();
        currentFireParticle.transform.SetParent(positionSpawnProjetctil);
        inputFieldCurrentCapacityAmmo.text = currentAmountAmmo.ToString();
        inputFieldCurrentCapacityAmmoOutWeapon.text = currentAmountAmmoOutWeapon.ToString();
    }

    // Update is called once per frame
    protected void Update()
    {
        waitToFireAfterReload -= Time.deltaTime;
        if(waitToFireAfterReload < 0)
        {
            waitToFireAfterReload = 0;
        }
        inputFieldCurrentCapacityAmmo.text = currentAmountAmmo.ToString();
        inputFieldCurrentCapacityAmmoOutWeapon.text = currentAmountAmmoOutWeapon.ToString();
        currentfireRateTime += Time.deltaTime;
        if (currentfireRateTime > fireRateTime)
            canFire = true;
        else
            canFire = false;
    }

    public void Fire()
    {
        if (currentAmountAmmo > 0 && canFire && waitToFireAfterReload == 0)
        {
            Vector3 startPosition = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width / 2, Screen.height / 2, 0));
            OnFire(damage, range, startPosition);
            currentAmountAmmo--;
            currentfireRateTime = 0;
            currentFireParticle.Play();
            WeaponsAnimation("Fire");
        }
    }

    public void Reload()
    {
        OnReload();
        int needAmmo = capacityAmmo - currentAmountAmmo;

        if (currentAmountAmmoOutWeapon > needAmmo)
        {
            if (currentAmountAmmo < capacityAmmo)
            {
                currentAmountAmmoOutWeapon -= needAmmo;
                currentAmountAmmo += needAmmo;
                //if (needAmmo.Equals(capacityAmmo))
                //    WeaponsAnimation("ReloadEmpty");
                //else
                    WeaponsAnimation("Reload");
            }
        }
        else
        {
            currentAmountAmmo += currentAmountAmmoOutWeapon;
            currentAmountAmmoOutWeapon = 0;
        }
        waitToFireAfterReload += reloadTime;
    }

    public void EnableZoom()
    {
        Camera.main.fieldOfView = aimZoom;
    }

    public void DisableZoom()
    {
        Camera.main.fieldOfView = startZoom;
    }

    public void WeaponsAnimation(string animationName)
    {
        gameObject.GetComponentInChildren<Animation>().Play(animationName);
    }

    public Object InstatiateDecalShotPrefab(RaycastHit hitInfo)
    {
        float ofsset = 0.009f;
        Quaternion hitRotation = Quaternion.LookRotation(-hitInfo.normal);
        //Instancia o prefab DecalShot
        return Instantiate(decalShotPrefab, hitInfo.point + (hitInfo.normal * ofsset), hitRotation);
    }

    protected abstract void OnFire(int damage, float range, Vector3 startPosition);
    protected abstract void OnReload();
}
